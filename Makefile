#build
build-dev:
	docker build . -t arsenal-pay -f Dockerfile

build-prod:
	docker build . -t arsenal-pay -f Dockerfile.production

#run
frontend:
	docker run -p 4200:4200 \
			-d \
			--rm \
			--name arsenal-pay \
			-v /Users/a.mayatskiy/projects/work/arsenal-pay-vuejs-gitlab/src:/app/src \
			arsenal-pay

#stop
stop:
	docker stop frontend


#compose
dev:
	docker-compose -f docker-compose.yml up -d

prod:
	docker-compose -f docker-compose.production.yml up -d

down:
	docker-compose down


#ssh
ssh:
	ssh root@194.58.111.237

copy-files:
	scp -r ./* root@194.58.111.237:/root/app

#example build for linux
#docker buildx build . --platform linux/amd64 -t mayatskiy/arsenal-pay:linux -f Dockerfile.production
#docker run -p 80:80 --rm -d  --name frontend mayatskiy/arsenal:prod



fix:
	npm run prettier:fix
